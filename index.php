<?PHP

/* 
* This file is part of MobCMS (Mobile Content Management System).
* Licensed under the terms of the GNU Lesser General Public License (LGPL).
* 
* @Link http://mobcms.ru/
* @Author MobCMS 
* @Copyright MobCMS - 2014
 */

/*
* Название каталога системных файлов ядра.
*/
$system = 'system';

/*
* Название каталога модулей CMS.
*/
$modules = 'modules';

// Корнем определяем текущую папку
define('ROOT', realpath(dirname(__FILE__)).DIRECTORY_SEPARATOR);

// Назначение имени для каталога системы
define('SYSPATH', ROOT . $system . '/');

// Назначение имени для каталога модулей
define('MODPATH', ROOT . $modules . '/');

//Время запуска скрипта в миллисекундах
define('TIME_START', microtime(true)); 

// Очищаем использованные переменные
unset($system, $modules);

/*
* Подключение основного файла системы
*/
require SYSPATH . 'bootstrap.php';


$obj = new Route(array('path' => 'home', 'action' => 'index'));
$route = $obj->get();

//print_r($route);

if (is_dir(MODPATH . $route['path'])) {
    
    // Если есть Класс контроллер, то подключаем его
    if (is_dir(MODPATH . $route['path'] . '/classes/Autoloader.php')) {
        include MODPATH . $route['path'] . '/classes/Autoloader.php';
        spl_autoload_register(array('Module_Autoloader', 'loadSystem'));
    }
    
    // Подключаем файл запуска модуля
    if (is_file(MODPATH . $route['path'] . '/init.php')) {
        include MODPATH . $route['path'] . '/init.php';
    }
    
} else {
    die(__('Ошибка 404: Запрашиваемый документ не найден.'));
}
