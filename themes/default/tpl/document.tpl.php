<!DOCTYPE html>
<html lang="<?= SYSLANG?>">
<head>
    <meta charset="utf-8">
    <meta name="description" content="MobCMS система управления мобильным сайтом"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes">
    <meta name="keywords" content="mobcms, wap cms, вапцмс, вап цмс, моб цмс, мобильная цмс, цмс для wap сайта"/>
    <link rel="stylesheet" href="/public/bootstrap/css/bootstrap.css" type="text/css" media="screen, handheld"/>
    <link rel="stylesheet" href="/public/bootstrap/css/bootstrap-responsive.css" type="text/css" media="screen, handheld"/>
    <link rel="stylesheet" href="/themes/default/css/main.css" type="text/css" media="screen, handheld"/>
    <link rel="stylesheet" href="/themes/default/css/ic.css" type="text/css" media="screen, handheld"/>
    <title><?= $title?></title>
    <link rel="alternate" type="application/rss+xml" title="RSS подписка" href="http://mobcms.ru/rss"/>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="/public/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>

<div class="navbar">
  <div class="navbar-inverse">
    <div class="container-fluid">
      
        <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="navbar-brand"><i class="ic-win8 ic-brand"></i> MobCMS</span>
                </a>
                <ul class="dropdown-menu nav-win8" role="menu">
                    <li><a href="#"><i class="ic-rss-1"></i> <span class="dropdown-text"><?= __('Новости')?></span></a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="ic-comment"></i> <span class="dropdown-text"><?= __('Форум')?></span></a></li>
                    <li><a href="#"><i class="ic-chat-inv"></i> <span class="dropdown-text"><?= __('Гостевая')?></span></a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="ic-picture-1"></i> <span class="dropdown-text"><?= __('Фотоальбомы')?></span></a></li>
                    <li><a href="#"><i class="ic-arrows-cw"></i> <span class="dropdown-text"><?= __('Обменник')?></span></a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="ic-users"></i> <span class="dropdown-text"><?= __('Пользователи')?></span></a></li>
                </ul>
            </li>
        </ul>

      <div class="nav-collapse collapse">
        <ul class="nav navbar-nav pull-right">
          <li>
            <a href="#">
              <i class="ic-login-1"></i>
              <?= __('Вход')?>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="ic-pencil-alt"></i>
              <?= __('Регистрация')?>
            </a>
          </li>
        </ul>
      </div>
      
    </div>
  </div>
</div>
    
<div class="titlebar">
    <div class="titlebar-text"><?= $title?></div>
</div>
    
<div class="content"> 
    <ul class="nav nav-pills nav-stacked widget">
        <li><a href="#"><i class="ic-rss"></i> <?= __('Наши новости')?></a></li>
        <li class="widget-info"><?= __('Общение')?></li>
        <li><a href="#"><i class="ic-comment"></i> <?= __('Форум')?><span class="badge pull-right">0</span></a></li>
        <li><a href="#"><i class="ic-chat-inv"></i> <?= __('Гостевая')?><span class="badge pull-right">0</span></a></li>
        <li class="widget-info"><?= __('Интересное')?></li>
        <li><a href="#"><i class="ic-picture-1"></i> <?= __('Фотоальбомы')?><span class="badge pull-right">0</span></a></li>
        <li><a href="#"><i class="ic-arrows-cw"></i> <?= __('Файлообменник')?><span class="badge pull-right">0</span></a></li>
        <li class="widget-info"><?= __('Еще..')?></li>
        <li><a href="#"><i class="ic-users"></i> <?= __('Пользователи')?><span class="badge pull-right">0</span></a></li>
    </ul>
</div>

<div class="footer">
    <div class="text-center text-primary">
        <div>&copy; <a href="http://mobcms.ru">MobCMS.Ru</a> - 2014</div>
    </div>

    <div class="text-center"><?= __('Генерация страницы')?>: <?= $doc_gen_time?></div>
</div>

<div class="text-right">
    <a href="/home/?lang=ru"><?= __('Ru')?></a>
    <a href="/home/?lang=en"><?= __('En')?></a> 
</div>
</body>
</html>