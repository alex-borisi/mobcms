<?php defined('SYSPATH') OR die('No direct script access.');

/* 
* This file is part of MobCMS (Mobile Content Management System).
* Licensed under the terms of the GNU Lesser General Public License (LGPL).
* 
* @Link http://mobcms.ru/
* @Author MobCMS 
* @Copyright MobCMS - 2014
 */

// Старт сессии
session_start();

// Название сессии
session_name('SESS');

/**
 * Константы глобальных переменных
 */

define('URI', $_SERVER['REQUEST_URI']);

/*
* Автозагрузчик классов системы
*/
require SYSPATH . 'classes/Autoloader.php';
spl_autoload_register(array('Autoloader', 'loadSystem'));

 
$system = array(); 
$system['theme'] = 'default';

if (isset($_GET['lang']) && ($_GET['lang'] == 'ru' OR $_GET['lang'] = 'en')) {
    $_SESSION['language'] = ($_GET['lang'] == 'ru' ? 'ru' : 'en');
}


if (isset($_SESSION['language']) && ($_SESSION['language'] == 'ru' OR $_SESSION['language'] = 'en')) {
    $system['language'] = $_SESSION['language'];
} else {
    $system['language'] = 'ru';
}

define('SYSLANG', $system['language']);

/*
* функция __() выводит переведенную строку 
* Если такой строки нету в словаре, то выводит значение $string
*/
if (!function_exists('__')) 
{ 
    function __($string) 
    { 
        return I18n::get($string, SYSLANG); 
    } 
} 

$system['title'] = 'MobCMS - WAP движок для Вашего сайта';
define('SYSTHEM', ROOT . 'themes/' . $system['theme'] . '/');
