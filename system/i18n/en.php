<?php

/* 
* This file is part of MobCMS (Mobile Content Management System).
* Licensed under the terms of the GNU Lesser General Public License (LGPL).
* 
* @Link http://mobcms.ru/
* @Author MobCMS 
* @Copyright MobCMS - 2014
 */

return array(
    'Наши новости' => 'News',
    'Новости' => 'News',
    'Форум' => 'Forum',
    'Гостевая' => 'Guest',
    'Фотоальбомы' => 'Photo Albums',
    'Файлообменник' => 'File Sharing',
    'Обменник' => 'Sharing',
    'Пользователи' => 'Users',
    'Вход' => 'Login',
    'Регистрация' => 'Registration',
    'Общение' => 'Community',
    'Интересное' => 'Interesting',
    'Еще..' => 'More..',
    'Регистрация' => 'Registration',
    'MobCMS - WAP движок для Вашего сайта' => 'MobCMS - Mobile content management system',
);    