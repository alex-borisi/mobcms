<?php defined('SYSPATH') OR die('No direct script access.');

/* 
* This file is part of MobCMS (Mobile Content Management System).
* Licensed under the terms of the GNU Lesser General Public License (LGPL).
* 
* @Link http://mobcms.ru/
* @Author MobCMS 
* @Copyright MobCMS - 2014
 */

class I18n 
{ 
    /** задаем язык классу **/ 
    public static $lang = 'ru'; 
    
    /** задаем путь к папке где лежат файлы с языками **/ 
    public static $path = 'i18n'; 
    
    /** кеширование **/ 
    protected static $cache = array(); 
     
    /** задаем язык. Пример I18n::lang('ru') **/ 
    public static function lang($lang) 
    { 
        I18n::$lang = $lang; 
        return I18n::$lang; 
    } 
     
    /** функция отвечающая за вывод **/ 
    public static function get($string, $lang = NULL) 
    { 
        if (!$lang) { 
            $lang = I18n::$lang; 
        } 
        $table = I18n::load($lang); 
        
        if (isset($table[$string])) { 
            return $table[$string]; 
        } 
        return $string; 
    } 
     
    /** функция отвечающая за загрузку файла с переводами **/ 
    public static function load($lang) 
    { 
        if (isset(I18n::$cache[$lang])) { 
            return I18n::$cache[$lang]; 
        } 
        
        $table = array(); 
        $path = SYSPATH . '/' . I18n::$path . DIRECTORY_SEPARATOR . $lang . '.php';
        
        if (file_exists($path)) { 
            $table = array_merge_recursive($table, include($path)); 
        } 
        return I18n::$cache[$lang] = $table; 
    } 
} 