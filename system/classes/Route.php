<?php defined('SYSPATH') OR die('No direct script access.');

/* 
* This file is part of MobCMS (Mobile Content Management System).
* Licensed under the terms of the GNU Lesser General Public License (LGPL).
* 
* @Link http://mobcms.ru/
* @Author MobCMS 
* @Copyright MobCMS - 2014
 */

class Route {
    
    protected $defaults = array('path' => 'home', 'action' => 'index');
    
    public function __construct($defaults) 
    { 
        $this->defaults = $defaults; 
    }
    
    public function get() 
    {
        $uri = preg_replace('#//+#', '/', rtrim(URI, '/'));
        
        $array = explode('/', $uri);
        
        if (count($array) >= 2) {
            $return = array('path' => $array[1]);
        } else {
            $return = $this->defaults($this->defaults);
        }
        
        return $return; 
    }
    
    function defaults($string) 
    {
        return $string;
    }
}