<?php

/* 
* This file is part of MobCMS (Mobile Content Management System).
* Licensed under the terms of the GNU Lesser General Public License (LGPL).
* 
* @Link http://mobcms.ru/
* @Author MobCMS 
* @Copyright MobCMS - 2014
 */
  

class Init extends Template
{
    public $title = 'Новая страница';
    public $description = '';
    public $keywords = array();
    
    public function __construct() {
        
    }
    
    public function display($tpl_file) 
    {
        // Заголовки страниц
        $this->assign('title', $this->title);
        $this->assign('description', $this->description);
        $this->assign('keywords', $this->keywords);
        $this->assign('doc_gen_time', round(microtime(true) - TIME_START, 3));
        
        // Формируем шаблон и отправляем в браузер
        return $this->getTemplate($tpl_file);
    }
    
}