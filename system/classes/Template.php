<?php defined('SYSPATH') OR die('No direct script access.');

/* 
* This file is part of MobCMS (Mobile Content Management System).
* Licensed under the terms of the GNU Lesser General Public License (LGPL).
* 
* @Link http://mobcms.ru/
* @Author MobCMS 
* @Copyright MobCMS - 2014
 */

class Template
{   
    protected $vars = array();
    
    public function assign($varname, $value)
    {
        $this->vars[$varname] = $value;
        return true;
    }

    /*
    * Генерируем шаблон
    */
    protected function getTemplate($tpl_file) 
    {
        // Помещаем переменные в шаблон
        extract($this->vars);
        
        /**
         * Ищем шаблон в теме оформления, если нету файла, то берем 
         * по умолчанию из системы, если и там нет, то выдаем ошибку.
         */
        if (is_file(SYSTHEM . 'tpl/' . $tpl_file . '.tpl.php')) {
            include SYSTHEM . 'tpl/' . $tpl_file . '.tpl.php';
        } elseif (is_file(SYSPATH . 'template/' . $tpl_file . '.tpl.php')) {
            include SYSPATH . 'template/' . $tpl_file . '.tpl.php';
        } else {
            die(__('Ошибка при загрузке шаблона'));
        }
    }
}