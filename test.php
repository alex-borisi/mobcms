<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="description" content="MobCMS система управления мобильным сайтом"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, user-scalable=yes">
    <meta name="keywords" content="mobcms, wap cms, вапцмс, вап цмс, моб цмс, мобильная цмс, цмс для wap сайта"/>
    <link rel="stylesheet" href="/public/bootstrap/css/bootstrap.css" type="text/css" media="screen, handheld"/>
    <link rel="stylesheet" href="/public/bootstrap/css/bootstrap-responsive.css" type="text/css" media="screen, handheld"/>
    <link rel="stylesheet" href="/themes/default/css/main.css" type="text/css" media="screen, handheld"/>
    <link rel="stylesheet" href="/themes/default/css/ic.css" type="text/css" media="screen, handheld"/>
    <title>MobCMS - система управления сайтом</title>
    <link rel="alternate" type="application/rss+xml" title="RSS подписка" href="http://mobcms.ru/rss"/>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>
    <script src="/public/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>

<div class="navbar">
  <div class="navbar-inverse">
    <div class="container-fluid">
      
        <button type="button" class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>

        <ul class="nav navbar-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="navbar-brand"><i class="ic-win8 ic-brand"></i> MobCMS</span>
                </a>
                <ul class="dropdown-menu nav-win8" role="menu">
                    <li><a href="#"><i class="ic-rss-1"></i> <span class="dropdown-text">Новости</span></a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="ic-comment"></i> <span class="dropdown-text">Форум</span></a></li>
                    <li><a href="#"><i class="ic-chat-inv"></i> <span class="dropdown-text">Гостевая</span></a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="ic-picture-1"></i> <span class="dropdown-text">Фотоальбомы</span></a></li>
                    <li><a href="#"><i class="ic-arrows-cw"></i> <span class="dropdown-text">Обменник</span></a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="ic-users"></i> <span class="dropdown-text">Пользователи</span></a></li>
                </ul>
            </li>
        </ul>

      <div class="nav-collapse collapse">
        <ul class="nav navbar-nav pull-right">
          <li>
            <a href="#">
              <i class="ic-login-1"></i>
              Вход
            </a>
          </li>
          <li>
            <a href="#">
              <i class="ic-pencil-alt"></i>
              Регистрация
            </a>
          </li>
        </ul>
      </div>
      
    </div>
  </div>
</div>
    
<div class="titlebar">
    <div class="titlebar-text">Hello World!</div>
</div>
    
<div class="content"> 
    <ul class="nav nav-pills nav-stacked widget">
        <li><a href="#"><i class="ic-rss-1"></i> Наши новости</a></li>
        <li class="widget-info">Общение</li>
        <li><a href="#"><i class="ic-comment"></i> Форум<span class="badge pull-right">0</span></a></li>
        <li><a href="#"><i class="ic-chat-inv"></i> Гостевая<span class="badge pull-right">0</span></a></li>
        <li class="widget-info">Интересное</li>
        <li><a href="#"><i class="ic-picture-1"></i> Фотоальбомы<span class="badge pull-right">0</span></a></li>
        <li><a href="#"><i class="ic-arrows-cw"></i> Файлообменник<span class="badge pull-right">0</span></a></li>
        <li class="widget-info">Еще..</li>
        <li><a href="#"><i class="ic-users"></i> Пользователи<span class="badge pull-right">0</span></a></li>
    </ul>
</div>
    
<div class="text-center text-primary">
    <div>&copy; <a href="http://mobcms.ru">MobCMS.Ru</a> - 2014</div>
</div>
</body>
</html>