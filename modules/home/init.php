<?php defined('SYSPATH') OR die('No direct script access.');

/* 
* This file is part of MobCMS (Mobile Content Management System).
* Licensed under the terms of the GNU Lesser General Public License (LGPL).
* 
* @Link http://mobcms.ru/
* @Author MobCMS 
* @Copyright MobCMS - 2014
 */

$init = new Init();
$init->title = __($system['title']);

$init->display('document');
